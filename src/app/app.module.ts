import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { BananaView } from "./routes/BananaView";
import { BananeView } from "./routes/BananeView";
import { HomeView } from "./routes/HomeView";
import { Menu } from "./menu";

import { BananeService } from "./services/banane.service";
import { BananeModel } from "./services/banane.model";

import { Info } from "./routes/jednaBanana/info";
import { Edit } from "./routes/jednaBanana/edit";

import {HttpModule} from '@angular/http';
import { Delete } from "./routes/jednaBanana/delete";

import { NewBanana } from './routes/newBanana';

import {FormsModule} from '@angular/forms';
import { SearchPipe } from './search.pipe';

const routs: Routes = [
  { path: "", component: HomeView },
  { path: "banane", component: BananeView },
  { path: "banana/new", component: NewBanana},
  {
    path: "banane/:id",
    component: BananaView,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: Edit },
      { path: "info", component: Info },
      { path: "delete", component: Delete }
    ]
  },
  { path: "**", redirectTo: "/" }
];

@NgModule({
  declarations: [
    AppComponent,
    BananaView,
    BananeView,
    HomeView,
    Menu,
    Info,
    Edit,
    Delete, 
    NewBanana, SearchPipe
  ],
  imports: [BrowserModule, FormsModule,
            RouterModule.forRoot(routs), 
            HttpModule],

  providers: [BananeService, BananeModel],
  bootstrap: [AppComponent]
})
export class AppModule {}
