export interface Banana{
    id:number,
    nutritionValue:number,
    weight:string,
    color?:string
}