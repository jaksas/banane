import { Component } from '@angular/core';
import {ActivatedRoute } from '@angular/router'

@Component({
    templateUrl:'./banana.html'
})
export class BananaView{

    id;

    constructor(private route:ActivatedRoute){
        this.route.params.subscribe((paramsObject) => 
    this.id = paramsObject['id'])
    }
}