import { Component } from '@angular/core';
import { BananeModel } from '../services/banane.model';
import { Router } from '@angular/router';
import {colors} from '../configs/colors';

@Component({
    templateUrl:'./newBanana.html'
})
export class NewBanana{

    constructor(private bananaModel:BananeModel, private router:Router){
        
    }

    colors=colors;

    banana={
        
    };

    dodajBananu(){
        if(this.banana['weight'] && this.banana['nutritionValue']){
        this.bananaModel.dodajBananu(this.banana, ()=>{
            this.router.navigate(['/banane'])
        });
        } else {
            alert('Nije sve definisano!')
        }
    }

}