import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { colors } from "../../configs/colors";
import { BananeModel } from "../../services/banane.model";

@Component({
  templateUrl: "./edit.html"
})
export class Edit implements OnInit {
  id;

  constructor(
    private route: ActivatedRoute,
    private ruter: Router,
    private model: BananeModel
  ) {}

  banana = {};

  colors = colors;

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      //this.id=params.id
      this.model.dajMiBananuPoIdu(params.id, a => {
        console.log(a);
        this.banana = a;
      });
    });
  }
  sacuvajBananu() {
    this.model.updateBanana(this.banana, () => {
      this.ruter.navigate(["/banane"]);
    });
  }
}
