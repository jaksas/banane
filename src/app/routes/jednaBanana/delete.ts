import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BananeModel} from '../../services/banane.model';


@Component({
    templateUrl:'./delete.html'
})
export class Delete implements OnInit{
    id;

    constructor(
        private route:ActivatedRoute,
        private router:Router,
        private bananeModel:BananeModel
    ){
        
    }

    obrisiBananu(){
        this.bananeModel.obrisiBananu(this.id, ()=> {
            this.router.navigate(['/banane'])
        });
    }

    ngOnInit(){
        this.route.parent.params.subscribe((params)=>{
            this.id=params.id
        });
    }
}