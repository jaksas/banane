import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BananeModel } from "../../services/banane.model";

@Component({
  templateUrl: "./info.html"
})
export class Info implements OnInit {
  id;

  constructor(
    private route: ActivatedRoute,
    private ruter: Router,
    private model: BananeModel
  ) {}

  banana = {};

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      //this.id=params.id
      this.model.dajMiBananuPoIdu(params.id, a => {
        console.log(a);
        this.banana = a;
      });
    });
  }
}
