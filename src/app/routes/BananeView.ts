import { Component } from '@angular/core';
import { BananeModel } from '../services/banane.model';
import {Router} from '@angular/router'

@Component({
    templateUrl:'./Banane.html'
})
export class BananeView{
    constructor(public bananeModel:BananeModel, private router:Router){

        
    }
    otvoriBananu(id){
        this.router.navigate(['/banane', id])

    } 
    editujBananu(id){
        this.router.navigate(['/banane', id, 'edit'])

    } 
    obrisiBananu(id){
        this.router.navigate(['/banane', id, 'delete'])

    } 

}