import { Injectable } from "@angular/core";
import { Banana } from "../interfaces/banana";
import { BananeService } from "./banane.service";

@Injectable()
export class BananeModel {
  getBananeObservable: any;
  banane = [];

  constructor(private service: BananeService) {
    this.refreshujBanane();
  }

  refreshujBanane() {
    this.service
      .getBananeObservable()
      .subscribe(banana => (this.banane = banana));
  }

  refreshujBananeSaClbkom(clbk) {
    this.service
      .getBananeObservable()
      .subscribe(banana => {
        this.banane = banana;
        clbk();
      });
    
  }

  obrisiBananu(id, callBackFunkcija) {
    this.service.deleteBananaObservable(id).subscribe(() => {
      this.refreshujBanane();
      callBackFunkcija();
    });
  }

  dodajBananu(banana, clbk) {
    this.service.addBananaObservable(banana).subscribe(banana => {
      this.banane.push(banana);
      clbk();
    });
  }

  dajMiBananuPoIdu(id, clbk) {
    if (this.banane && this.banane.length > 0) {
      for (var i = 0; i < this.banane.length; i++) {
        if (parseInt(this.banane[i].id) === parseInt(id)) {
          clbk(this.banane[i]);
        }
      }
    } else {
      this.refreshujBananeSaClbkom(() => {
        for (var i = 0; i < this.banane.length; i++) {
          if (parseInt(this.banane[i].id) === parseInt(id)) {
            clbk(this.banane[i]);
          }
        }
      });
    }
  }

  updateBanana(banana, clbk) {
    this.service.updateBananaObservable(banana).subscribe(banana => {
      this.refreshujBanane();
      clbk();
    });
  }
}
