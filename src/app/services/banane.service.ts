import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class BananeService {
  constructor(private http: Http) {}

  getBananeObservable() {
    return this.http.get("http://localhost:3000/banane").map(response => {
      return response.json();
    });
  }
  getBananaObservable(id) {
    return this.http.get("http://localhost:3000/banane/"+id).map(response => {
      return response.json();
    });
  }
  addBananaObservable(banana) {
    return this.http.post("http://localhost:3000/banane", banana).map(response => {
      return response.json();
    });
  }
  updateBananaObservable(banana) {
    return this.http.put("http://localhost:3000/banane/"+banana.id, banana).map(response => {
      return response.json();
    });
  }

  deleteBananaObservable(id) {
    return this.http.delete("http://localhost:3000/banane/"+id).map(response => {
      return response.json();
    });
  }
}