import{Component} from '@angular/core'

@Component({
    selector:'menu-list',
    templateUrl:'./menu-list.html'
})

export class Menu{
    links=[
        {
            naziv:'Home',
            putanja:'/',
            active:false,
        },
        {
            naziv:'Banane',
            putanja:'/banane',
            active:false,
        },
        {
            naziv:'Dodaj Bananu',
            putanja:'/banana/new',
            active:false,
        }
    ]
}